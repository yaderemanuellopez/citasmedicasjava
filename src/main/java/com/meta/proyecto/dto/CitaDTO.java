package com.meta.proyecto.dto;

import java.io.Serializable;
import java.util.Date;

/*@JsonIdentityInfo(
		  generator =ObjectIdGenerators.IntSequenceGenerator.class , 
		  property = "@id")*/
public class CitaDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String motivoCita;
	private Date fechaHora;
	private Integer diagnostico;
	private String paciente;
	private String medico;
	
	public CitaDTO(Integer id, String motivocita,Date fechaHora, Integer diagnostico, String paciente, String medico ) {
		this.id = id;
		this.fechaHora= fechaHora;
		this.motivoCita = motivocita;
		this.diagnostico = diagnostico;
		this.paciente = paciente;
		this.medico = medico;
	}
	
	public CitaDTO() {	
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMotivoCita() {
		return motivoCita;
	}

	public void setMotivoCita(String motivoCita) {
		this.motivoCita = motivoCita;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Integer getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(Integer diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getPaciente() {
		return paciente;
	}

	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}
	
	//@JsonBackReference

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getMedico() {
		return medico;
	}

	public void setMedico(String medicoId) {
		this.medico = medicoId;
	}
	
	
}
