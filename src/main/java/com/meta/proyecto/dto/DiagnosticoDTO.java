package com.meta.proyecto.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(
		  generator =ObjectIdGenerators.IntSequenceGenerator.class , 
		  property = "@id")
public class DiagnosticoDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String valoracionEspecialista;
	private String enfermedad;
	private Integer cita;
	
	public DiagnosticoDTO(Integer id, String valoracionEspecialista, String enfermedad,Integer cita) {
		this.id = id;
		this.valoracionEspecialista = valoracionEspecialista;
		this.enfermedad = enfermedad;
		this.cita = cita;
	}
	
	public DiagnosticoDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getValoracionEspecialista() {
		return valoracionEspecialista;
	}

	public void setValoracionEspecialista(String valoracionEspecialista) {
		this.valoracionEspecialista = valoracionEspecialista;
	}

	public String getEnfermedad() {
		return enfermedad;
	}

	public void setEnfermedad(String enfermedad) {
		this.enfermedad = enfermedad;
	}

	public Integer getCita() {
		return cita;
	}

	public void setCita(Integer cita) {
		this.cita = cita;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
