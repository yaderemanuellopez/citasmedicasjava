package com.meta.proyecto.dto;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(
		  generator =ObjectIdGenerators.IntSequenceGenerator.class , 
		  property = "@id")
public class MedicoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	private String dni;
	private String usuario;
	private String clave;
	private String nombre;
	private String apellidos;
	private String numColegiado;
	private String especialidad;
	private List<CitaDTO> citas;
	private List<PacienteDTO> pacientes;

	
	public MedicoDTO(String dni, String usuario, String clave, String nombre, String apellidos, String numColegiado, String especialidad,List<CitaDTO> citas,List<PacienteDTO> pacientes) {
		this.dni=dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.usuario = usuario;
		this.clave = clave;
		this.numColegiado = numColegiado;
		this.especialidad = especialidad;
		this.citas = citas;
		this.pacientes = pacientes;
	}
	
	public MedicoDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNumColegiado() {
		return numColegiado;
	}

	public void setNumColegiado(String numColegiado) {
		this.numColegiado = numColegiado;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	//@JsonManagedReference
	public List<CitaDTO> getCitas() {
		return citas;
	}

	public void setCitas(List<CitaDTO> citas) {
		this.citas = citas;
	}

	public List<PacienteDTO> getPacientes() {
		return pacientes;
	}

	public void setPacientes(List<PacienteDTO> pacientes) {
		this.pacientes = pacientes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
