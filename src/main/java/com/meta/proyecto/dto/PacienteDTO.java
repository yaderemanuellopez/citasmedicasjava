package com.meta.proyecto.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@JsonIdentityInfo(
		  generator =ObjectIdGenerators.IntSequenceGenerator.class , 
		  property = "@id")
public class PacienteDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String dni;
	private String usuario;
	private String clave;
	private String nombre;
	private String apellidos;
	private String nss;
	private String numTarjeta;
	private String telefono;
	private String direccion;
    private List<CitaDTO> citas;
	private List<MedicoDTO> medicos;
	
	public PacienteDTO(String dni, String usuario, String clave, String nombre, String apellidos, String nss, String numTarjeta, String telefono, String direccion,List<CitaDTO> citas,List<MedicoDTO> medicos) {
		this.dni=dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.usuario = usuario;
		this.clave = clave;
		this.nss = nss;
		this.numTarjeta = numTarjeta;
		this.telefono = telefono;
		this.direccion = direccion;
		this.citas = citas;
		this.medicos = medicos;
	}

	public PacienteDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNss() {
		return nss;
	}

	public void setNss(String nss) {
		this.nss = nss;
	}

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public List<CitaDTO> getCitas() {
		return citas;
	}

	public void setCitas(List<CitaDTO> citas) {
		this.citas = citas;
	}

	public List<MedicoDTO> getMedicos() {
		return medicos;
	}

	public void setMedicos(List<MedicoDTO> medicos) {
		this.medicos = medicos;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
