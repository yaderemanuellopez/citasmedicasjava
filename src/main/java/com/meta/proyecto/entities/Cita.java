package com.meta.proyecto.entities;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

@Entity
@Table(name="cita")
public class Cita{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	@Column(name="motivoCita")
	private String motivoCita;
	@Column(name="fecha")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date fechaHora;
	@OneToOne(mappedBy = "cita",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Diagnostico diagnostico;
	@ManyToOne()
    @JoinColumn(name = "paciente_id")
	private Paciente paciente;
	@ManyToOne()
    @JoinColumn(name = "medico_id")
	private Medico medico;
	
	
	public Cita(Date fechaHora, String motivocita) {
		this.fechaHora= fechaHora;
		this.motivoCita = motivocita;
	}
	
	public Cita() {
		// TODO Auto-generated constructor stub
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public String getMotivoCita() {
		return motivoCita;
	}

	public void setMotivoCita(String motivoCita) {
		this.motivoCita = motivoCita;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Diagnostico getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(Diagnostico diagnostico) {
		this.diagnostico = diagnostico;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}
	
	
	
}
