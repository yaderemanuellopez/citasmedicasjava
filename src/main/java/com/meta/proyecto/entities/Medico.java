package com.meta.proyecto.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "medico")
@PrimaryKeyJoinColumn(referencedColumnName = "dni")
public class Medico extends Usuario {

	@Column(name = "num_colegiado",nullable = false)
	private String numColegiado;
	@Column(name = "especialidad",nullable = false)
	private String especialidad;
	@OneToMany(mappedBy = "medico")
    private List<Cita> citas;
	@ManyToMany(cascade = {
	        CascadeType.PERSIST,
	        CascadeType.MERGE
	    })
	@JoinTable(name = "medico_paciente",joinColumns = @JoinColumn(name = "medico_id"),
				inverseJoinColumns = {@JoinColumn(name = "paciente_id")})
	private List<Paciente> pacientes;
	
	public Medico(String dni, String usuario, String clave, String nombre, String apellidos , String numColegiado, String especialidad) {
		super(dni, nombre, apellidos, usuario, clave);
		this.numColegiado = numColegiado;
		this.especialidad = especialidad;
	}
	
	public Medico() {
		
	}

	public String getNumColegiado() {
		return numColegiado;
	}

	public void setNumColegiado(String numColegiado) {
		this.numColegiado = numColegiado;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public List<Cita> getCitas() {
		return citas;
	}

	public void setCitas(List<Cita> citas) {
		this.citas = citas;
	}

	public List<Paciente> getPacientes() {
		return pacientes;
	}

	public void setPacientes(List<Paciente> pacientes) {
		this.pacientes = pacientes;
	}

	
	
}
