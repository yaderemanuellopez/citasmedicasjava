package com.meta.proyecto.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.util.*;

@Entity
@Table(name = "paciente")
@PrimaryKeyJoinColumn(referencedColumnName = "dni")
public class Paciente extends Usuario {

	@Column(name = "nss")
	private String nss;
	@Column(name = "num_tarjeta")
	private String numTarjeta;
	@Column(name = "telefono")
	private String telefono;
	@Column(name = "direccion")
	private String direccion;
	@OneToMany(mappedBy = "paciente")
    private List<Cita> citas;
	@ManyToMany(mappedBy = "pacientes")
	private List<Medico> medicos;
	
	public Paciente(String dni, String usuario, String clave, String nombre, String apellidos, String nss, String numTarjeta, String telefono, String direccion) {
		super(dni, nombre, apellidos, usuario, clave);
		this.nss = nss;
		this.numTarjeta = numTarjeta;
		this.telefono = telefono;
		this.direccion = direccion;
	}
	
	public Paciente() {
		// TODO Auto-generated constructor stub
	}
	
	public String getNss() {
		return nss;
	}

	public void setNss(String nss) {
		this.nss = nss;
	}

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public List<Cita> getCitas() {
		return citas;
	}

	public void setCitas(List<Cita> citas) {
		this.citas = citas;
	}

	public List<Medico> getMedicos() {
		return medicos;
	}

	public void setMedicos(List<Medico> medicos) {
		this.medicos = medicos;
	}

	

	
}
