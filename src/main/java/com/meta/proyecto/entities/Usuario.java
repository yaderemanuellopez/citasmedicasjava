package com.meta.proyecto.entities;

import javax.persistence.*;

@Entity
@Table(name="usuario")
@Inheritance(strategy = InheritanceType.JOINED)
public class Usuario {
		@Id
		@Column(name="dni",nullable = false)
		private String dni;
		@Column(name = "usuario")
		private String usuario;
		@Column(name="clave",nullable = false )
		private String clave;
		@Column(name="nombre",nullable = false)
		private String nombre;
		@Column(name="apellidos",nullable = false)
		private String apellidos;
		
		
		public Usuario(String dni, String usuario, String clave, String nombre, String apellidos) {
			this.dni=dni;
			this.nombre = nombre;
			this.apellidos = apellidos;
			this.usuario = usuario;
			this.clave = clave;
			
		}

		public Usuario() {}
		
		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getApellidos() {
			return apellidos;
		}

		public void setApellidos(String apellidos) {
			this.apellidos = apellidos;
		}

		public String getUsuario() {
			return usuario;
		}

		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}

		public String getClave() {
			return clave;
		}

		public void setClave(String clave) {
			this.clave = clave;
		}

		public String getDni() {
			return dni;
		}

		public void setDni(String dni) {
			this.dni = dni;
		}
		
}
