package com.meta.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.meta.proyecto.entities.*;

public interface CitaDAO extends JpaRepository<Cita, Integer>{

}