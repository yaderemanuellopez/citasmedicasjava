package com.meta.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.meta.proyecto.entities.Diagnostico;

public interface DiagnosticoDAO extends JpaRepository<Diagnostico, Integer>{

}
