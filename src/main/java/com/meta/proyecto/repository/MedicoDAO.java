package com.meta.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.meta.proyecto.entities.Medico;

public interface MedicoDAO extends JpaRepository<Medico, String> {

}
