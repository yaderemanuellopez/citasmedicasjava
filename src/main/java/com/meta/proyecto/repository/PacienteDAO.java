package com.meta.proyecto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.meta.proyecto.entities.Paciente;

public interface PacienteDAO extends JpaRepository<Paciente, String> {

}
