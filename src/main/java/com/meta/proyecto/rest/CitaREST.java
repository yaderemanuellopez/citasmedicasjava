package com.meta.proyecto.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.meta.proyecto.dto.CitaDTO;
import com.meta.proyecto.services.ICitaService;

@RestController
@RequestMapping("/citas")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class CitaREST {
	@Autowired
	private ICitaService citaService;
	
	@GetMapping
    public List<CitaDTO> listarUsuarios(){
        return citaService.obtenerCitas();
    }
	
	@PostMapping
	public void insertar(@RequestBody CitaDTO cita) {
		if(cita!=null)
			citaService.insertarCita(cita);
	}
	
	@PutMapping
	public void actualizar(@RequestBody CitaDTO cita) {
		if(cita!=null && cita.getId() != null)
			citaService.modificarCita(cita);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		if(id != null && citaService.existeCita(id))
			citaService.eliminarCita(id);
	}
}
