package com.meta.proyecto.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meta.proyecto.dto.DiagnosticoDTO;
import com.meta.proyecto.services.IDiagnosticoService;

@RestController
@RequestMapping("/diagnosticos")
public class DiagnosticoREST {
	@Autowired
	private IDiagnosticoService diagnosticoService;
	
	@GetMapping
    public List<DiagnosticoDTO> listarUsuarios(){
        return diagnosticoService.obtenerDiagnosticos();
    }
	
	@PostMapping
	public void insertar(@RequestBody DiagnosticoDTO diagnostico) {
		diagnosticoService.insertarDiagnostico(diagnostico);
	}
	
	@PutMapping
	public void actualizar(@RequestBody DiagnosticoDTO diagnostico) {
		diagnosticoService.modificarDiagnostico(diagnostico);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") int id) {
		diagnosticoService.eliminarDiagnostico(id);
	}
}
