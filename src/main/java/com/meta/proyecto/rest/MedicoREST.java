package com.meta.proyecto.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.meta.proyecto.dto.MedicoDTO;
import com.meta.proyecto.services.IMedicoService;

@RestController
@RequestMapping("/medicos")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class MedicoREST {
	
	@Autowired
	private IMedicoService medicoService;
	
	@GetMapping
    public List<MedicoDTO> listarUsuarios(){
        return medicoService.obtenerMedicos();
    }
	
	@PostMapping
	public void insertar(@RequestBody MedicoDTO medico) {
		medicoService.insertarMedico(medico);
		
	}
	
	@PutMapping
	public void actualizar(@RequestBody MedicoDTO medico) {
		medicoService.modificarMedico(medico);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") String id) {
		medicoService.eliminarMedico(id);
	}
}
