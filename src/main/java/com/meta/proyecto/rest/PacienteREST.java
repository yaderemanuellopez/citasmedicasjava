package com.meta.proyecto.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.meta.proyecto.dto.PacienteDTO;
import com.meta.proyecto.services.IPacienteService;

@RestController
@RequestMapping("/pacientes")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class PacienteREST {
	
	@Autowired
	private IPacienteService pacienteService;
	
	@GetMapping
    public List<PacienteDTO> listarUsuarios(){
        return pacienteService.obtenerPacientes();
    }
	
	@PostMapping
	public void insertar(@RequestBody PacienteDTO paciente) {
		pacienteService.insertarPaciente(paciente);
	}
	
	@PutMapping
	public void actualizar(@RequestBody PacienteDTO paciente) {
		pacienteService.modificarPaciente(paciente);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") String id) {
		pacienteService.eliminarPaciente(id);
	}
}
