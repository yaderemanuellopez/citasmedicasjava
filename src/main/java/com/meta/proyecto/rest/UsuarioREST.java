package com.meta.proyecto.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meta.proyecto.dto.UsuarioDTO;
import com.meta.proyecto.services.IUsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioREST {

	@Autowired
	private IUsuarioService usuarioService;
	
	@GetMapping
    public List<UsuarioDTO> listarUsuarios(){
        //retornará todos los usuarios
        return usuarioService.obtenerUsuarios();
    }
	
	@PostMapping
	public void insertar(@RequestBody UsuarioDTO usuario) {
		usuarioService.insertarUsuario(usuario);
	}
	
	@PutMapping
	public void actualizar(@RequestBody UsuarioDTO usuario) {
		usuarioService.modificarUsuario(usuario);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") String id) {
		usuarioService.eliminarUsuario(id);
	}

}
