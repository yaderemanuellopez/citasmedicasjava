package com.meta.proyecto.services;

import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.meta.proyecto.dto.CitaDTO;
import com.meta.proyecto.entities.Cita;
import com.meta.proyecto.repository.CitaDAO;

@Service
public class CitaService implements ICitaService {

	@Autowired
	private CitaDAO citaRepositorio; 
	
	@Autowired
	private MedicoService medicoService; 
	
	@Autowired
	private PacienteService pacienteService;
	
	@Autowired
	private DiagnosticoService diagnosticoService;
	
	@Autowired
    private ModelMapper modelMapper;
			
	@Override
	public void insertarCita(CitaDTO cita) {
		if(cita.getId() == null || !existeCita(cita.getId()))
			citaRepositorio.save(dtoToCita(cita));	
	}

	@Override
	public CitaDTO obtenerCita(Integer id) {
		if (existeCita(id))
			return citaToDto(citaRepositorio.getOne(id));
		return null;
	}

	@Override
	public void eliminarCita(Integer id) {
		if (existeCita(id))
			citaRepositorio.deleteById(id);
		
	}

	@Override
	public void modificarCita(CitaDTO cita) {
		if (existeCita(cita.getId()))
			citaRepositorio.save(dtoToCita(cita));
	}

	@Override
	public List<CitaDTO> obtenerCitas() {
		return citaRepositorio.findAll()
				.stream()
				.map(cita -> citaToDto(cita))
				.collect(Collectors.toList());
	}

	@Override
	public CitaDTO citaToDto(Cita cita) {
		return modelMapper.typeMap(Cita.class, CitaDTO.class).addMapping(origen -> origen.getMedico().getDni(), CitaDTO::setMedico)
										.addMapping(origen -> origen.getDiagnostico().getId(), CitaDTO::setDiagnostico)
										.addMapping(origen -> origen.getPaciente().getDni(), CitaDTO::setPaciente).map(cita);
	}

	@Override
	public Cita dtoToCita(CitaDTO cita) {
		Cita citaMapeada = modelMapper.map(cita, Cita.class);
		if(cita.getMedico() != null)
			citaMapeada.setMedico(medicoService.dtoToMedico(medicoService.obtenerMedico(cita.getMedico())));
		if(cita.getPaciente() != null)
			citaMapeada.setPaciente(pacienteService.dtoToPaciente(pacienteService.obtenerPaciente(cita.getPaciente())));
		if(cita.getDiagnostico() != null)
			citaMapeada.setDiagnostico(diagnosticoService.dtoToDiagnostico(diagnosticoService.obtenerDiagnostico(cita.getDiagnostico())));
		return citaMapeada;
	}

	@Override
	public boolean existeCita(Integer citaId) {
		return citaRepositorio.existsById(citaId);
	}

}
