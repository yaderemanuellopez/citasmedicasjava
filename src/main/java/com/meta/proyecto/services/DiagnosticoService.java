package com.meta.proyecto.services;

import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.meta.proyecto.dto.DiagnosticoDTO;
import com.meta.proyecto.entities.Diagnostico;
import com.meta.proyecto.repository.DiagnosticoDAO;

@Service
public class DiagnosticoService implements IDiagnosticoService {

	@Autowired
	private DiagnosticoDAO diagnosticoRepositorio; 
	
	@Autowired
	private CitaService citaService;
	
	@Autowired
    private ModelMapper modelMapper;
    
	@Override
	public void insertarDiagnostico(DiagnosticoDTO diagnostico) {
		if(diagnostico.getId() != null && !existeDiagnostico(diagnostico.getId()))
			diagnosticoRepositorio.save(dtoToDiagnostico(diagnostico));			
	}

	@Override
	public DiagnosticoDTO obtenerDiagnostico(Integer id) {
		if(id != null && existeDiagnostico(id))
			return diagnosticoToDto(diagnosticoRepositorio.getOne(id));
		return null;
	}

	@Override
	public void eliminarDiagnostico(Integer id) {
		if(id != null && existeDiagnostico(id))
			diagnosticoRepositorio.deleteById(id);
		
	}

	@Override
	public void modificarDiagnostico(DiagnosticoDTO diagnostico) {
		if(diagnostico.getId() != null && existeDiagnostico(diagnostico.getId()))
			diagnosticoRepositorio.save(dtoToDiagnostico(diagnostico));			
	}

	@Override
	public List<DiagnosticoDTO> obtenerDiagnosticos() {
		return diagnosticoRepositorio.findAll()
				.stream()
				.map(diagnostico -> diagnosticoToDto(diagnostico))
				.collect(Collectors.toList());
	}

	@Override
	public DiagnosticoDTO diagnosticoToDto(Diagnostico diagnostico) {
		return modelMapper.typeMap(Diagnostico.class, DiagnosticoDTO.class).addMapping(origen -> origen.getCita().getId(), DiagnosticoDTO::setCita).map(diagnostico);
	}

	@Override
	public Diagnostico dtoToDiagnostico(DiagnosticoDTO diagnostico) {
		Diagnostico diagnosticoMapeado = modelMapper.map(diagnostico, Diagnostico.class);
		if(diagnostico.getCita() != null)
			diagnosticoMapeado.setCita(citaService.dtoToCita(citaService.obtenerCita(diagnostico.getCita())));
		return diagnosticoMapeado;
		}

	@Override
	public boolean existeDiagnostico(Integer diagnosticoId) {
		return diagnosticoRepositorio.existsById(diagnosticoId);
	}

}
