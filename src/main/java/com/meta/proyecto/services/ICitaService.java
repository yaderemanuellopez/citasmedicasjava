package com.meta.proyecto.services;

import java.util.List;

import com.meta.proyecto.dto.CitaDTO;
import com.meta.proyecto.entities.Cita;

public interface ICitaService {
	public void insertarCita(CitaDTO cita);
	 
	public CitaDTO obtenerCita(Integer id);
	
	public void eliminarCita(Integer id);
	
	public void modificarCita(CitaDTO cita);
	
	public List<CitaDTO> obtenerCitas();
	
	public CitaDTO citaToDto(Cita cita);
	
	public Cita dtoToCita(CitaDTO cita);
	
	public boolean existeCita(Integer citaId);
}
