package com.meta.proyecto.services;

import java.util.List;

import com.meta.proyecto.dto.DiagnosticoDTO;
import com.meta.proyecto.entities.Diagnostico;

public interface IDiagnosticoService {

	public void insertarDiagnostico(DiagnosticoDTO diagnostico);
	 
	public DiagnosticoDTO obtenerDiagnostico(Integer id);
	
	public void eliminarDiagnostico(Integer id);
	
	public void modificarDiagnostico(DiagnosticoDTO diagnostico);
	
	public List<DiagnosticoDTO> obtenerDiagnosticos();
	
	public DiagnosticoDTO diagnosticoToDto(Diagnostico diagnostico);
	
	public Diagnostico dtoToDiagnostico(DiagnosticoDTO diagnostico);
	
	public boolean existeDiagnostico(Integer diagnosticoId);
}
