package com.meta.proyecto.services;

import java.util.List;

import com.meta.proyecto.dto.MedicoDTO;
import com.meta.proyecto.entities.Medico;

public interface IMedicoService {
	
	public void insertarMedico(MedicoDTO medico);
	 
	public MedicoDTO obtenerMedico(String id);
	
	public void eliminarMedico(String id);
	
	public void modificarMedico(MedicoDTO medico);
	
	public List<MedicoDTO> obtenerMedicos();
	
	public MedicoDTO medicoToDto(Medico medico);
	
	public Medico dtoToMedico(MedicoDTO medico);
	
	public boolean existeMedico(String medicoId);
}
