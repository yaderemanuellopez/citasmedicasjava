package com.meta.proyecto.services;

import java.util.List;

import com.meta.proyecto.dto.PacienteDTO;
import com.meta.proyecto.entities.Paciente;

public interface IPacienteService {
	
	public void insertarPaciente(PacienteDTO paciente);
	 
	public PacienteDTO obtenerPaciente(String id);
	
	public void eliminarPaciente(String id);
	
	public void modificarPaciente(PacienteDTO paciente);
	
	public List<PacienteDTO> obtenerPacientes();
	
	public PacienteDTO pacienteToDto(Paciente paciente);
	
	public Paciente dtoToPaciente(PacienteDTO paciente);

	public boolean existePaciente(String pacienteId);
}
