package com.meta.proyecto.services;

import java.util.List;

import com.meta.proyecto.dto.UsuarioDTO;
import com.meta.proyecto.entities.Usuario;

public interface IUsuarioService {

	public void insertarUsuario(UsuarioDTO usuario);
	 
	public UsuarioDTO obtenerUsuario(String id);
	
	public void eliminarUsuario(String id);
	
	public void modificarUsuario(UsuarioDTO usuario);
	
	public List<UsuarioDTO> obtenerUsuarios();
	
	public UsuarioDTO usuarioToDto(Usuario usuario);
	
	public Usuario dtoToUsuario(UsuarioDTO usuario);
	
	public boolean existeUsuario(String usuarioId);
}
