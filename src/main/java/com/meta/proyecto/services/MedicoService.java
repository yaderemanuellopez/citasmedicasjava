package com.meta.proyecto.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meta.proyecto.dto.MedicoDTO;
import com.meta.proyecto.entities.Medico;
import com.meta.proyecto.repository.MedicoDAO;

@Service
public class MedicoService implements IMedicoService{

	@Autowired
	private MedicoDAO medicoRepositorio;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public void insertarMedico(MedicoDTO medico) {
		if(medico.getDni() != null && !existeMedico(medico.getDni()))
			medicoRepositorio.save(dtoToMedico(medico));
		
	}

	@Override
	public MedicoDTO obtenerMedico(String id) {
		if(id != null && existeMedico(id))
			return medicoToDto(medicoRepositorio.getOne(id)) ;
		return null;
	}

	@Override
	public void eliminarMedico(String id) {
		if(id != null && existeMedico(id))
			medicoRepositorio.deleteById(id);
		
	}

	@Override
	public void modificarMedico(MedicoDTO medico) {
		if(medico.getDni() != null && existeMedico(medico.getDni()))
			medicoRepositorio.save(dtoToMedico(medico));
		
	}

	@Override
	public List<MedicoDTO> obtenerMedicos() {
		return medicoRepositorio.findAll()
				.stream()
				.map(medico -> medicoToDto(medico))
				.collect(Collectors.toList());
	}

	@Override
	public MedicoDTO medicoToDto(Medico medico) {
		return modelMapper.map(medico, MedicoDTO.class); 
	}

	@Override
	public Medico dtoToMedico(MedicoDTO medico) {
		return modelMapper.map(medico, Medico.class); 
	}

	@Override
	public boolean existeMedico(String medicoId) {
		return medicoRepositorio.existsById(medicoId);
	}

}
