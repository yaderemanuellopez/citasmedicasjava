package com.meta.proyecto.services;

import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meta.proyecto.dto.PacienteDTO;
import com.meta.proyecto.entities.Paciente;
import com.meta.proyecto.repository.PacienteDAO;

@Service
public class PacienteService implements IPacienteService{

	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
	private PacienteDAO pacienteRepositorio;
	
	@Override
	public void insertarPaciente(PacienteDTO paciente) {
		if(paciente.getDni() != null && !existePaciente(paciente.getDni()))
			pacienteRepositorio.save(dtoToPaciente(paciente));		
	}

	@Override
	public PacienteDTO obtenerPaciente(String id) {
		if(id != null && existePaciente(id))
			return pacienteToDto(pacienteRepositorio.getOne(id));
		return null;
	}

	@Override
	public void eliminarPaciente(String id) {
		if(id != null && existePaciente(id))
			pacienteRepositorio.deleteById(id);
		
	}

	@Override
	public void modificarPaciente(PacienteDTO paciente) {
		if(paciente.getDni() != null && existePaciente(paciente.getDni()))
			pacienteRepositorio.save(dtoToPaciente(paciente));
		
	}

	@Override
	public List<PacienteDTO> obtenerPacientes() {
		return pacienteRepositorio.findAll()
				.stream()
				.map(paciente -> pacienteToDto(paciente))
				.collect(Collectors.toList());
	}

	@Override
	public PacienteDTO pacienteToDto(Paciente paciente) {
		return modelMapper.map(paciente, PacienteDTO.class); 

	}

	@Override
	public Paciente dtoToPaciente(PacienteDTO paciente) {
		return modelMapper.map(paciente, Paciente.class); 

	}

	@Override
	public boolean existePaciente(String pacienteId) {
		return pacienteRepositorio.existsById(pacienteId);
	}

}
