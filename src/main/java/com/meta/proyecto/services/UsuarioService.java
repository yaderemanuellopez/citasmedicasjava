package com.meta.proyecto.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meta.proyecto.dto.UsuarioDTO;
import com.meta.proyecto.entities.Usuario;
import com.meta.proyecto.repository.UsuarioDAO;

@Service
public class UsuarioService implements IUsuarioService {

	@Autowired
	private UsuarioDAO usuarioRepositorio;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public void insertarUsuario(UsuarioDTO usuario) {
		if(usuario.getDni() != null && !
				existeUsuario(usuario.getDni()))
			usuarioRepositorio.save(dtoToUsuario(usuario));
	}

	@Override
	public UsuarioDTO obtenerUsuario(String id) {
		if(id != null && existeUsuario(id))
			return usuarioToDto(usuarioRepositorio.getOne(id)) ;
		return null;
	}
	

	@Override
	public void eliminarUsuario(String id) {
		if(id != null && existeUsuario(id))
			usuarioRepositorio.deleteById(id);
	}
	
	@Override
	public void modificarUsuario(UsuarioDTO usuario) {
		if(usuario.getDni() != null && existeUsuario(usuario.getDni()))
			usuarioRepositorio.save(dtoToUsuario(usuario));
	}

	@Override
	public List<UsuarioDTO> obtenerUsuarios() {
		return usuarioRepositorio.findAll()
				.stream()
				.map(user -> usuarioToDto(user))
				.collect(Collectors.toList());
	}

	@Override
	public UsuarioDTO usuarioToDto(Usuario usuario) {
		return modelMapper.map(usuario, UsuarioDTO.class); 
	}
	
	@Override
	public Usuario dtoToUsuario(UsuarioDTO usuario) {
		return modelMapper.map(usuario, Usuario.class); 
	}
	
	@Override
	public boolean existeUsuario(String usuarioId) {
		return usuarioRepositorio.existsById(usuarioId);
	}
}
